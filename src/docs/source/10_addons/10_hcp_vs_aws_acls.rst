Access Control Lists
====================

This document gives some insight on how to set ACLs for buckets as well as
objects. In the Pre-Requisites section it also talks about how to setup HCP to
be able to follow the examples provided below. If you use Amazon S3 or a
different compatible S3 Store, use the intended tool to set it up instead.

Pre-Requisites for HCP HS3
--------------------------

*   Setup a new Tenant for use as S3 store:

    *   Create a new Tenant

        *   assign proper Hard- and Namespace-quota
        *   enable these features:

            *   Replication - **if wanted**
            *   Retention Mode Selection - **No**
            *   Search - **if wanted**
            *   Service Plan Selection - **NO** (but select a proper Service Plan)
            *   Versioning - **YES**

    *   Log in with the initial user, then:

        *   update the user's password
        *   add **Monitor** and **Admin roles** to the initial user
            *(Security / Users)*
        *   enable **MAPI** *(Security / MAPI)*

        *   configure defaults for new Namespaces (aka Buckets)
            *(Configuration / Namespace Defaults)*

            *   set a proper Hard-quota
            *   enable **Versioning**
            *   set **Prune versions older than** to a proper number of days

        *   create two new users (**test, antitest**) for data access

            *   check **Allow namespace management**
            *   do **not** check any of the roles (Monitor, Administrator etc.)!
            *   use **test01** and **antitest01** as the passwords at this
                time
            *   (manually) calculate the *Access_Key* and the
                *Security_Access_Key* for the users::

                    $ echo -n test | base64
                    dGVzdA==
                    $ echo -n test01 | md5
                    0e698a8ffc1a0af622c7b4db3cb750cc

                    $ echo -n antitest | base64
                    YW50aXRlc3Q=
                    $ echo -n antitest01 | md5
                    b57502fa4e33555bfd1e8adc4deb58ee


Prepare **hs3sh**
-----------------

We'll use **hs3sh** for the following tasks. For that, you need to configure
profiles for each of *test* and *antitest* and the newly created HCP Tenant in
the **.hs3sh.conf** file (see :doc:`../20_configuration` on how to do that)::

    [s3erlei_test]
    comment = Tenant 's3erlei' at hcp73, user test
    endpoint = s3erlei.hcp73.archivas.com
    https = yes
    aws_access_key_id = dGVzdA==
    aws_secret_access_key = 0e698a8ffc1a0af622c7b4db3cb750cc

    [s3erlei_antitest]
    comment = Tenant 's3erlei' at hcp73, user antitest
    endpoint = s3erlei.hcp73.archivas.com
    https = yes
    aws_access_key_id = YW50aXRlc3Q=
    aws_secret_access_key = b57502fa4e33555bfd1e8adc4deb58ee


Create buckets in HCP HS3
-------------------------

Start **hs3sh**::

    $ hs3sh
    HS3 Shell
    =========
    -->

Connect using one of the profiles and create a bucket::

    --> connect s3erlei_test
    --> bucket -c snomis-testbucket
    --> lsb -a
              created owner|ID               grantee region         bucket name                         ACLs
    ----------------- ------------------------------ -------------- ----------------------------------------
    16/03/16-11:42:00 test                           compatible     snomis-test
                                                test                                            FULL_CONTROL
    --> attach snomis-testbucket
    --> put ../README.rst readme.txt
    VersionId = 93320232949249
    --> ls
        last modified            size version_id                       object name
    ----------------- --------------- -------------------------------- ----------------------------
    16/03/16-11:43:59           2,586 93320232949249                   readme.txt

We also wrote a small file.

Work with bucket ACLs
---------------------

We can now use *antitest* to see if we can access that bucket::

    --> connect s3erlei_antitest
    --> lsb
              created owner|ID                       region         bucket name
    ----------------- ------------------------------ -------------- ----------------------------------------

We can't list buckets, as we don't own any - good.

::

    --> attach snomis-testbucket
    --> ls
        last modified            size version_id                       object name
    ----------------- --------------- -------------------------------- ----------------------------
    error: ls failed
        hint: An error occurred (AccessDenied) when calling the
        ListObjects operation: Access Denied

And we also can't list the *snomis-testbucket*, as we haven't been granted
any access to it. Good as well.

Now let's set a *canned ACL* that allows all authenticated users to read the
*snomis-testbucket*::

    --> connect s3erlei_test

    --> lsb -a
              created owner|ID               grantee region         bucket name                         ACLs
    ----------------- ------------------------------ -------------- ----------------------------------------
    16/03/16-11:43:44 test                           compatible     snomis-testbucket
                                                test                                            FULL_CONTROL
    --> acl -b "snomis-testbucket" -s "public-read"
    --> lsb -a
              created owner|ID               grantee region         bucket name                         ACLs
    ----------------- ------------------------------ -------------- ----------------------------------------
    16/03/16-11:43:44 test                           compatible     snomis-testbucket
                                            AllUsers                                                    READ
                                                test                                            FULL_CONTROL

Now we can again use *antitest* to verify that we have access::

    --> connect s3erlei_antitest
    --> lsb
              created owner|ID                       region         bucket name
    ----------------- ------------------------------ -------------- ----------------------------------------

Hm, we don't see any buckets, as before. Why that? Well, we don't own a bucket,
so no buckets will shown. But as we have read permission for *snomis-testbucket*
now, we can attach to it::

    --> attach snomis-testbucket
    --> ls
        last modified            size version_id                       object name
    ----------------- --------------- -------------------------------- ----------------------------
    16/03/16-11:43:59           2,586 93320232949249                   readme.txt

And we now can see what's in there. Let's try to read that object::

    --> get readme.txt
    -------------------------------------------------------------------------------
    HS3 Shell
    =========

    The HS3 Shell (**hs3sh**) is a command processor created to interact with Amazon
    S3 and compatible storage services.
    [..]

Perfect. Writing an object fails, of course::

    --> put ../README.rst myreadme.txt
    error: put failed...
        hint: An error occurred (AccessDenied) when calling the PutObject
        operation: Access Denied

Now, let's see what has happend to HCP when we set the *canned ACL*:

    .. image:: ../_static/HCP_canned_ACL_01.png

Due to assigning an ACL with the *canned ACL* of *public-read* to the bucket,
we set the *Browse* and *Read* permission for it, which is reflected in what HCP
shows in its management UI for the bucket in question.


In addition to the *canned ACL* of *public-read*, we can also give *antitest*
explicitely the right to write objects (again done by the bucket owner,
*test*)::

    --> connect s3erlei_test
    --> acl -b "snomis-testbucket" -g antitest write
    --> lsb -a
              created owner|ID               grantee region         bucket name                         ACLs
    ----------------- ------------------------------ -------------- ----------------------------------------
    16/03/16-11:43:44 test                           compatible     snomis-testbucket
                                            AllUsers                                                    READ
                                                test                                            FULL_CONTROL
                                            antitest                                                   WRITE

Switching back to *antitest* we're able to write an object, now::

    --> connect s3erlei_antitest
    --> attach snomis-testbucket
    --> put ../README.rst antireadme.txt
    VersionId = 93320517364353
    --> ls
        last modified            size version_id                       object name
    ----------------- --------------- -------------------------------- ----------------------------
    16/03/16-12:58:04           2,586 93320517364353                   antireadme.txt
    16/03/16-11:43:59           2,586 93320232949249                   readme.txt

Back to HCP, we find the write permission *test* gave to *antitest* in
*antitest*'s permission settings:

    .. image:: ../_static/HCP_canned_ACL_02.png

As you can see, no *Browse* and *Read* permissions are set here, as *Write*
permission doesn't include them.

Result of what we did is that *antitest* has *Read* permission (granted by the
*canned ACL*) as well as *Write* permission (through the individual grant).


Work with object ACLs
---------------------

First, let's remove the *canned ACL* as well as the *Write* permission we gave
to *antitest*::

    --> connect s3erlei_test
    --> acl -b "snomis-testbucket" -s private
    --> lsb -a
              created owner|ID               grantee region         bucket name                         ACLs
    ----------------- ------------------------------ -------------- ----------------------------------------
    16/03/16-11:43:44 test                           compatible     snomis-testbucket
                                                test                                            FULL_CONTROL
    --> ls -a
        last modified            size version_id                       object name             ACLs
    ----------------- --------------- -------------------------------- ----------------------------
    16/03/16-12:58:04           2,586 93320517364353                   antireadme.txt
    16/03/16-11:43:59           2,586 93320232949249                   readme.txt
                                                                  test                 FULL_CONTROL

Hm, the object *antitest* wrote befor doesn't have any ACL - why that? Well, as
the object was created, it inherited the permissions that were granted to
*antitest* at that time. As we took away those grants, the incerited grants
went away, as well. User *test* has access to it, as a bucket owner always has
FULL_CONTROL over the bucket's content. Let's try to read that file::

    --> get antireadme.txt
    -------------------------------------------------------------------------------
    HS3 Shell
    =========

    The HS3 Shell (**hs3sh**) is a command processor created to interact with Amazon
    S3 and compatible storage services.
    [..]

So, that's fine. Now lets give *antitest* the permission to read the object he
created, again::

    --> acl -o antitest -g antitest read
    --> ls -a anti
        last modified            size version_id                       object name             ACLs
    ----------------- --------------- -------------------------------- ----------------------------
    16/03/16-14:29:52           2,586 93320860763393                   antireadme.txt
                                                                  test                 FULL_CONTROL
                                                              antitest                         READ

Let's see if *antitest* can access the object again::

    --> ls
        last modified            size version_id                       object name
    ----------------- --------------- -------------------------------- ----------------------------
    error: ls failed
        hint: An error occurred (AccessDenied) when calling the
        ListObjects operation: Access Denied

    --> get antireadme.txt
    -------------------------------------------------------------------------------
    HS3 Shell
    =========

    The HS3 Shell (**hs3sh**) is a command processor created to interact with Amazon
    S3 and compatible storage services.
    [..]

So, *antitest* can't list the bucket's content (which is OK, as he doesn't have
any permissions on the bucket), but he can read the object to which we granted
*Read* permission to him.

Using HCPs Namespace Browser, we can verify that an ACL has been set for that
object:

    .. image:: ../_static/HCP_object_ACL_01.png

Selecting the little **ACL** icon on the right of the row showing the object
called **antireadme.txt** will show us the ACL itself:

    .. image:: ../_static/HCP_object_ACL_02.png

