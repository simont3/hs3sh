Differences
===========

*   List bucket content:

    *   AWS, HCP, Cloudian::

            self.bucket.objects.filter(Prefix='') and
            self.bucket.object_versions.filter(Prefix='')

        works well

    *   with ECS, it doesn't - you need to use::

            self.bucket.objects.filter(Prefix='') and
            self.bucket.object_versions.filter(Prefix='')

*   Create bucket:

    *   AWS - needs LocationConstraint, of course

    *   Cloudian - ?

    *   HCP - ignores LocationConstraint, if given

    *   ECS - fails if LocationConstraint is given
