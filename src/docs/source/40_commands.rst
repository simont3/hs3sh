Commands
========


..  Tip::

    *   **Most commands support output redirection using "|", ">" and ">>".**

        ..  versionadded:: 0.1.11

    *   **Some commands might return with an error message like this**::

            --> put -m _files/100mbfile 100mbfile
            error: transfer failed...
                hint: An error occurred (NotImplemented) when calling the
                CreateMultipartUpload operation: The request requires
                functionality that is not implemented in the current release

        In most cases, the *hint* is related to the storage service connected.
        The error shown here for example is caused by the connected storage
        service not supporting multipart upload.


help
----

::

    --> help [command] | ? [command]

List the available commands or show help for the named command.

acl
---

::

    --> acl -b|-o name [-g user permission [, user permission]*] [-s canned ACL]

| Set Access Control Lists on a bucket or an object
| ``-b`` name = bucket
| ``-o`` name = object (needs to start with a "/")
| ``-g`` set per-user permission(s)
|        permissions is one of ``full_control``, ``write``, ``write_acp``,
|        ``read``, ``read_acp``
| ``-s`` set canned ACL
|        a canned ACLs is one of ``private``, ``public-read``,
|        ``public-read-write``, ``authenticated-read``
| ``user`` is either:
|          **for AWS**: a user's canonical ID
|          **for HCP**: a local HCP user name or an Active Directory user
|          (if HCP is integrated with AD) like this: ``aduser@domain.com``
|          **for other S3 storage**: refer to the respective manual on how to
|          specify its users
|          and **for all S3 stores**: one of ``allusers``, ``authenticateduser``,
|          ``logdelivery``

*   If neither ``-g`` nor ``-s`` is given, list the actual ACLs
*   ``-g`` and ``-s`` are exclusive
*   If any specifier holds characters outside the ascii alphabet and the
    underscore, surrond it with "quotation marks"

..  versionchanged:: 0.1.15

    object ACLs completed

attach
------

::

    --> attach [-h] bucket

    Attach the bucket to work with.

    positional arguments:
      bucket      the bucket's name

    optional arguments:
      -h, --help  show this help message and exit

..  versionchanged:: 0.1.5

    Renamed from *bucket*.

bucket
------

::

    --> bucket [-h] [-c | -r] [-v] bucket

    Create or remove a bucket, toggle versioning, show versioning setting when no options are set.

    positional arguments:
      bucket      the bucket's name

    optional arguments:
      -h, --help  show this help message and exit
      -c          create BUCKET
      -r          remove BUCKET
      -v          toggle versioning

    Without any flags, the bucket's versioning status is shown

..  versionadded:: 0.1.9

    Replaces *mkbucket* and *rmbucket*.

bye
----

::

    --> bye [-h]

    Exit hs3sh gracefully.

    optional arguments:
      -h, --help  show this help message and exit

clear
-----

::

    --> clear [-h]

    Clear screen.

    optional arguments:
      -h, --help  show this help message and exit

..  versionadded:: 1.2.1

config
------

::

    --> ?config
    usage: config [-h] [-s {mpu_size,mpu_threads}] [-v [VALUE]]

    Get or set configuration item values

    optional arguments:
      -h, --help            show this help message and exit
      -s {mpu_size,mpu_threads}
                            the configuration items to set
      -v [VALUE]            the (integer) value to set.

..  versionadded:: 1.1.3

..  versionchanged:: 1.2.1

    renamed from ``set``

connect
-------

::

    --> connect [-h] profile

    Connect to an S3 endpoint using profile profile.

    positional arguments:
      profile     a profile configured in the config file

    optional arguments:
      -h, --help  show this help message and exit

..  versionchanged:: 0.1.5

    Changed to use a profile to connect.

cp
--

::

    --> cp [-h] [-v VERSIONID] sourceobject targetobject [meta ...]

    Request the S3 service to server-side copy an object.

    positional arguments:
      sourceobject  name (key) of the object to copy
      targetobject  name (key) of the target
      meta          format is: "key:value". Proving meta will replace eventually existing metadata in sourceobject with what is given as meta in targetobject

    optional arguments:
      -h, --help    show this help message and exit
      -v VERSIONID  sourceobject's versionId. Will create a copy of the specified object version

debug
-----

::

    --> debug [-h] [command] [args ...]

    Without "cmd [args]", toggle debug mode globally, with "cmd [args]", toggle debug mode for that command, only.

    positional arguments:
      command     the command to run
      args        (optional) arguments

    optional arguments:
      -h, --help  show this help message and exit

get
---

::

    --> get [-h] [-m] [-v VERSIONID] object [localfile]

    Get (download) an object.

    positional arguments:
      object        name (key) of the object to download
      localfile     name of the local file to store the download (if not given, the source object's name will be used

    optional arguments:
      -h, --help    show this help message and exit
      -m            request a multipart download
      -v VERSIONID  sourceobject's versionId. Will download the specified version

..  versionchanged:: 1.1.3

    Added ``-m`` flag and enabled multipart download


getbl
-----

::

    --> getbl [-h] bucket

    Get the location for bucket.

    positional arguments:
      bucket      the bucket's name

    optional arguments:
      -h, --help  show this help message and exit

..  versionadded:: 1.2.0


lcd
---

::

    --> lcd [-h] [localdirectory]

    Change the local working directory to localdirectory (or to home directory, if localdirectory isn't given).

    positional arguments:
      localdirectory  the local directory

    optional arguments:
      -h, --help      show this help message and exit

..  versionadded:: 1.2.1


lls
---

::

    --> lls [-h] [localdirectory]

    List contents of local path.

    positional arguments:
      localdirectory  the local directory to be listed

    optional arguments:
      -h, --help      show this help message and exit

..  versionadded:: 1.2.1


lpwd
----

::

    --> lpwd [-h]

    Print the local working directory.

    optional arguments:
      -h, --help  show this help message and exit

..  versionadded:: 1.2.1


ls
--

::

     --> ls [-h] [-a] [-e] [-m] [-v] [prefix]

    List the objects within the active bucket.

    positional arguments:
      prefix      list objects starting with the prefix

    optional arguments:
      -h, --help  show this help message and exit
      -a          print object acls
      -e          print object etags
      -m          print object metadata
      -v          print object versions

..  versionchanged:: 1.1.7

    Added ``-e`` flag

..  versionchanged:: 0.1.9

    Added ``-a`` flag

lsb
---

::

    --> lsb [-h] [-a]

    List the buckets available through the connected endpoint.

    optional arguments:
      -h, --help  show this help message and exit
      -a          print bucket acls

..  versionchanged:: 0.1.4

    Added ``-a`` flag.

lsp
---

::

    --> lsp [-h]

    Show the configured profiles.

    optional arguments:
      -h, --help  show this help message and exit

..  versionadded:: 0.1.8


profile
-------

::

    --> profile [-h]

    Edit the configuration file(s).

    optional arguments:
      -h, --help  show this help message and exit

    This will start `vi` with the selected config file.

..  versionadded:: 1.2.3


progress
--------

::

    --> progress [-h]

    Toggle showing a progressmeter on/off.

    optional arguments:
      -h, --help  show this help message and exit

..  versionadded:: 1.2.1


put
---

::

    --> put [-h] [-m] sourcefile targetobject [meta ...]

    Put (upload) "sourcefile" as "targetobject" into the attached bucket, adding metadata pairs if specified.

    positional arguments:
      sourcefile    name of the local (source) file to upload.
      targetobject  name of the object in the target bucket.
      meta          format is: "key:value"

    optional arguments:
      -h, --help    show this help message and exit
      -m            do a multi-part upload, if ever possible.

..  versionchanged:: 0.1.12

    added multipart upload

quit
----

::

    --> quit [-h]

    Exit hs3sh gracefully.

    optional arguments:
      -h, --help  show this help message and exit

rm
--

::

    --> rm [-h] object [version_id]

    Delete object from the attached bucket.

    positional arguments:
      object      name of the object to remove.
      version_id  id of the object version to remove

    optional arguments:
      -h, --help  show this help message and exit

run
---

::

    --> run [-h] commandfile

    Run a batch of commands stored in commandfile. A commandfile is just a plain sequential list of commands to be run, as you would enter them intoan interactive session.
    Not ending it with "bye" will leavethe interactive session open.

    positional arguments:
      commandfile  name of the commandfile to run.

    optional arguments:
      -h, --help   show this help message and exit

..  versionadded:: 0.1.15


status
------

::

    --> status [-h]

    Show the session status (the connected endpoint and the attached bucket).

    optional arguments:
      -h, --help  show this help message and exit

..  versionchanged:: 0.1.5

    Adopted to work with profiles.


time
----

::

    --> time [-h] command [args ...]

    Measure the time command takes to complete.

    positional arguments:
      command     the command to run
      args        (optional) arguments

    optional arguments:
      -h, --help  show this help message and exit

..  versionadded:: 0.1.5


url
---

::

    --> url [-h] [-m MINUTES] [-u] object

    Generate a pre-signed URL to access an object.

    positional arguments:
      object      the object name (key)

    optional arguments:
      -h, --help  show this help message and exit
      -m MINUTES  set the expiration time for the URL to MINUTES (defaults to 60 minutes)
      -u          generates an upload URL instead of a download URL

..  versionadded:: 0.1.12

