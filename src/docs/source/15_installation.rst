Installation
============

Pre-built Executable
--------------------

For most modern *Linux* derivates you should be able to simply run the
executable provided
`here <https://gitlab.com/simont3/hs3sh/blob/master/src/dist/hs3sh.linux>`_.

There is also a binary for macOS, built and tested with macOS Sierra (10.12.6)
`here <https://gitlab.com/simont3/hs3sh/blob/master/src/dist/hs3sh.macos>`_.

Grab it there and move it to a folder in your $PATH (``/usr/local/bin``, for
example).


Self-built Executable
---------------------

In case the provided executable fails to run on your system, you can easily
build it on your own. Here's how to do that:

*   Clone the repository from
    `GitLab <https://gitlab.com/simont3/hs3sh.git>`_::

        $ git clone https://gitlab.com/simont3/hs3sh.git

*   Change into the project folder and create a Python 3 virtual environment
    and activate it::

        $ cd hs3sh/src
        $ python3 -m venv .venv
        $ source .venv/bin/activate

*   Update pip and setuptools, then load all required dev-packages::

        (.venv) $ pip install -U setuptools pip
        (.venv) $ pip install -r pip_requirements-dev.txt

*   Run the build tool::

        (.venv) $ pyinstaller hs3sh.spec

    You should find the executable in the ``dist`` subfolder.

    ..  Tip::

        Most likely, in ``hs3sh.spec``, you will have to adopt
        the path set in the ``_pathext`` variable to your setup!

*   Now move it to a folder in your $PATH (``/usr/local/bin``, for example).


Python virtual environment
--------------------------

*   Create a Python 3 virtual environment and activate it::

        $ python3 -m venv .venv
        $ source .venv/bin/activate

*   Install the **hs3sh** package::

        (.venv) $ pip install -U setuptools pip
        (.venv) $ pip install hs3sh

*   Now you can run **hs3sh** this way::

        (.venv) $ hs3sh

..  Note::

    Remember, every time you want to run **hs3sh**, you need to
    activate the virtual environment before!
