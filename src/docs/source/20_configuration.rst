Configuration
=============

Configuration data needed by **hs3sh** has to be configured in one (or both)
of two files:

    1.  **.hs3sh.conf** in the user's home directory (``~/.hs3sh.conf``)

        This is the preferred location. Make sure you set the access
        permissions correctly (``chmod 600 ~/.hs3sh.conf``)

    2.  **.hs3sh.conf** in the current directory (``./.hs3sh.conf``)

*   The configuration files are additional - **1.** is read before **2.**.
*   Profiles in **2.** will overwrite those in **1.** having the same name.

..  Tip::

    If you have routine task you always want to apply when starting **hs3sh**,
    you can create ``~/.hs3shrc``. Simply list the commands you require, one per
    line (no empty lines, no comments).

If you run **hs3sh** without having a configuration file, it will offer to
create a template file.

Copy the required template block(s) and edit the values to your need.

*   Allowed **type**\ s are:

    *   *aws* - Amazon S3
    *   *hs3* - Hitachi Content Platform
    *   *ecs* - Dell|EMC Elastic Cloud Storage
    *   *cloudian* - Cloudian
    *   *compatible* - other S3 compatible storage

*   If the S3 service you plan to connect to doesn't support **regions**, just
    leave the entry empty.

*   Optional entries **signature_version** (*s3* and *s3v4*) and
    **payload_signing_enabled** (*yes*/*no*) allow to configure the request signing
    method.

    ..  versionadded:: 1.1.0


You can have as much profiles as required, just make sure you don't have
duplicates!

Example::

    # Configuration file used by hs3sh
    [aws]
    type = aws
    comment = AWS Frankfurt, AIM user sm
    endpoint = s3.amazonaws.com
    region = eu-west-1
    https = yes
    port = 443
    signature_version = s3v4
    payload_signing_enabled = yes
    aws_access_key_id = AK..EQ
    aws_secret_access_key = 9R..Cj

    [cloudian]
    type = cloudian
    comment = local cloudian installation, user test
    endpoint = s3-lubu.cloudian.snomis.local
    region = lubu
    https = yes
    port = 443
    signature_version = s3
    aws_access_key_id = AG..F4
    aws_secret_access_key = 87..fa

    # Template profile for EMC ECS:
    # -------------------------------
    [ecs]
    type = ecs
    comment = user emccode @ ecs01
    endpoint = ns1.ecs01.snomis.local
    region =
    https = no
    port = 9020
    signature_version = s3
    aws_access_key_id = emccode
    aws_secret_access_key = 5J..9S

    # Template profile for Hitachi Content Platform:
    # ----------------------------------------------
    [hcp]
    type = hs3
    comment = Tenant 's3erlei' at hcp73, user test, http
    endpoint = s3erlei.hcp73.archivas.com
    region =
    https = yes
    port = 443
    signature_version = s3v4
    payload_signing_enabled = yes
    aws_access_key_id = d..=
    aws_secret_access_key = 0e..cc
