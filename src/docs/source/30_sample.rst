Sample Session
==============

::

    $ hs3sh
    ******************************* HS3 Shell v1.1.0 ******************************

                     Type "help" for a list of available commands.
           Most commands support output redirection using "|", ">" and ">>".

    --> help

    Documented commands (type help <topic>):
    ========================================
    acl     bucket  connect  debug  help  lsb  put   rm   status  url
    attach  bye     cp       get    ls    lsp  quit  run  time

    --> ? connect
    connect <profile_name>
        Connect to an S3 endpoint using profile <profile_name>
        from ~/.hs3sh.conf or ./.hs3sh.conf
    --> lsp
    C profile                         tag  value
    - -------------------------- --------- ---------------------------------------
      hcp81                       comment: Tenant 's3' at hcp81, user s3user, http
                                 endpoint: s3.hcp81.archivas.com
                                    https: False
      hcp81-motest                comment: Tenant 'motest' at hcp81, user motest, http
                                 endpoint: motest.hcp81.archivas.com
                                    https: True
      hcp82                       comment: Tenant 's3' at hcp82, user s3user, http
                                 endpoint: s3.hcp82.archivas.com
                                    https: False
      hcp83                       comment: Tenant 's3' at hcp83, user s3user, http
                                 endpoint: s3.hcp83.archivas.com
                                    https: False
      hcp84                       comment: Tenant 's3' at hcp84, user s3user, http
                                 endpoint: s3.hcp84.archivas.com
                                    https: False
      hcp_l                       comment: Tenant s3erlei, user s3user, hcp.archivas.com
                                 endpoint: s3erlei.hcp.archivas.com
                                    https: False
    --> connect hcp81
    --> lsb
              created owner|ID                       region         V bucket name
    ----------------- ------------------------------ -------------- - --------------------------------------
    17/12/19-10:46:59 s3user                         compatible     X five
    17/12/08-15:33:04 s3user                         compatible     X four
    17/12/05-15:09:56 s3user                         compatible     X one
    18/02/06-06:56:48 s3user                         compatible     X test
    18/02/08-09:29:35 s3user                         compatible       test1
    18/02/08-09:31:31 s3user                         compatible       test2
    18/03/05-15:01:50 s3user                         compatible     X test3
    18/03/23-08:46:06 s3user                         compatible       test4
    17/12/07-13:47:29 s3user                         compatible     X three
    17/12/05-15:10:02 s3user                         compatible     X two
    --> attach test2
    --> ls
        last modified            size version_id                           object name
    ----------------- --------------- ------------------------------------ ----------------------------
    18/03/28-14:50:33      52,428,800 null                                 50MB
    18/03/07-21:12:58             529 null                                 test
    18/03/28-14:48:34             970 null                                 ulp.txt
    18/03/28-14:36:25             970 null                                 ulp1
    18/03/28-14:36:54             970 null                                 ulp2
    18/03/28-14:37:16             970 null                                 ulp3
    18/03/28-14:42:12             970 null                                 ulp4
    18/03/28-14:44:05             970 null                                 ulp5
    18/03/28-14:47:15             970 null                                 ulp6
    18/03/28-14:47:35             970 null                                 ulp7
    18/03/28-14:47:59             970 null                                 ulp8
    --> put ../README.rst readme.rst title:readme 'content:good_to_know'
    sending file "../README.rst" of size 2665
    --> ls -avm readme.rst
        last modified            size version_id                   grantee A object name/metadata    ACLs
    ----------------- --------------- ------------------------------------ - ----------------------------

    18/03/29-07:34:07           2,665 97427766246849                       X readme.rst
                                                                    s3user                   FULL_CONTROL
                                                                             {'_content': "good_to_know'", 'title': 'readme'}
    --> quit
    Ending gracefully...
