(h)S3 Shell documentation (|release|)
=====================================

The (h)S3 Shell (**hs3sh**) is a command processor created to interact with Amazon
S3 and compatible storage services.

It's main intention is to test S3 compatible storage services against Amazon S3
without having to deal with the hard-to-remember parameters required by tools
like *awscli* or *s3curl.pl*.

Features

    *   Connect to Amazon S3 as well as compatible storage services
    *   Uses profiles for easy switching between S3 providers
    *   Create, discover and delete buckets
    *   Discover, write, read and copy objects
    *   Multipart upload
    *   Deal with metadata and versions
    *   Work with bucket and object ACLs
    *   Generate pre-signed URLs

Tested with

    *   Amazon S3
    *   Cloudian HyperStore
    *   EMC Elastic Cloud Storage
    *   Hitachi Content Platform

..  toctree::
    :maxdepth: 3

    15_installation
    20_configuration
    30_sample
    40_commands
    90_addons
    97_changelog
    95_todos
    98_license


*   :ref:`search`

