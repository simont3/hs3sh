# -*- mode: python -*-

import sys

if sys.platform == 'darwin':
    _pathext = ['.']
    _name = 'hs3sh.macos'
elif sys.platform == 'linux':
    _pathext = ['/vagrant/src']
    _name = 'hs3sh.linux'
else:
    sys.exit('{} is not supported, yet'.format(sys.platform))

block_cipher = None


a = Analysis(['hs3sh.py'],
             pathex=_pathext,
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name=_name,
          debug=False,
          strip=False,
          upx=True,
          console=True )
