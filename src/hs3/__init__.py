# The MIT License (MIT)
#
# Copyright (c) 2016-2021 Thorsten Simons (sw@snomis.eu)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import argparse
from textwrap import wrap
from click import secho

from hs3 import init


def parseargs():
    """
    args - build the argument parser, parse the command line and
           run the respective functions.
    """

    mainparser = argparse.ArgumentParser()
    mainparser.add_argument('--version', action='version',
                            version="%(prog)s: {0}\n"
                            .format(init.Gvars.Version))
    mainparser.add_argument('-d', dest='debug', action='store_true',
                            default=False,
                            help='enable debugging output '
                                 '(ugly and very chatty!)')

    result = mainparser.parse_args()
    return result


def calctime(t):
    """
    Calculate a string 'H:M:S.ms' out of a given no. of seconds.
    """
    msec = int("{0:.2f}".format(t % 1)[2:])
    minute = int(t // 60)
    sec = int(t % 60)
    hour = int(minute // 60)
    minute = int(minute % 60)
    return "{0:02}:{1:02}:{2:02}.{3}".format(hour, minute, sec, msec)


def _print(*args, err=False, nl='\n'):
    """
    Print strings, first line as it is, following lines wrapped to 78 chars.
    If err is set to True, output will go to stderr and colored red.

    :param string:  the string to print
    """

    _frst = True
    for l in args:
        if _frst:
            _frst = False
        else:
            l = '\n'.join(wrap(l, width=130, initial_indent='    ',
                               subsequent_indent='    '))

        if err:
            secho(l, fg='red', err=True, nl=nl)
        else:
            secho(l, nl=nl)


def _(string, width):
    """
    Return the string cut to num characters.

    :param string:  the string to work on
    :param width:   the number of characters wanted
    :return:        the cut string
    """
    string = string or ''
    return string if len(string) <= width else string[:width - 3] + '...'


def calcByteSize(nBytes, formLang=False):
    '''
    Return a given no. of Bytes in a human readable format.
    '''
    sz = ["B", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB"]
    szl = ["Byte", "Kilobyte", "Megabyte", "Gigabyte", "Terabyte",
           "Petabyte", "Exabyte", "Zettabyte", "Yottabyte"]
    i = 0
    neg = False
    if nBytes < 0:
        neg = True
        nBytes *= -1

    while nBytes > 1023:
                    nBytes = nBytes / 1024
                    i = i + 1
    if neg:
        nBytes *= -1
    if formLang:
        return "{0:.2f} {1:>3}".format(nBytes, szl[i])
    else:
        return "{0:.2f} {1:>3}".format(nBytes, sz[i])

