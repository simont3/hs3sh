#!/usr/bin/env python3.6

# based on this work: https://teppen.io/2018/10/23/aws_s3_verify_etags/

import os
import sys
from hashlib import md5, sha1, sha256, sha384, sha512
from argparse import ArgumentParser

parser = ArgumentParser(description='Compare an S3 MPU etag to a local file')
parser.add_argument('-a', metavar='alternative_hash',
                    required=False, default=None,
                    choices=['sha1', 'sha256', 'sha384', 'sha512'],
                    help='alternative hashing standard')
parser.add_argument('inputfile', help='local file')
parser.add_argument('etag', help='etag from s3')
args = parser.parse_args()

def factor_of_1MB(filesize, num_parts):
    x = filesize / int(num_parts)
    y = x % 1048576
    return int(x + (1048576 if y else 0) - y)

def calc_etag(inputfile, partsize):
    print('partsize = {}'.format(partsize))
    md5_digests = []
    add_digests = []
    with open(inputfile, 'rb') as f:
        for chunk in iter(lambda: f.read(partsize), b''):
            md5_digests.append(md5(chunk).digest())
            # alternative_hash wanted
            if args.a:
                add_digests.append(eval("{}(chunk).digest()".format(args.a)))
            print('\t{}\t{}'.format(md5(chunk).hexdigest().lower(),
                                    eval("{}(chunk).hexdigest().lower()".format(args.a)) if args.a else ''))

    print('calculated overall etag:')
    print('\t{}-{}\t{}'.format(md5(b''.join(md5_digests)).hexdigest().lower(),
                                  str(len(md5_digests)),
                                  eval("{}(b''.join(add_digests)).hexdigest().lower()+'-'+str(len(add_digests))".format(args.a)) if args.a else '',
                                  str(len(add_digests) if args.a else '')))

    return (md5(b''.join(md5_digests)).hexdigest() + '-' + str(len(md5_digests)),
            eval("{}(b''.join(add_digests)).hexdigest().lower()+'-'+str(len(add_digests))".format(args.a)) if args.a else '')

def possible_partsizes(filesize, num_parts):
    return lambda partsize: partsize < filesize and (float(filesize) / float(partsize)) <= num_parts

def main():
    filesize  = os.path.getsize(args.inputfile)
    num_parts = int(args.etag.split('-')[1])


    partsizes = [ ## Default Partsizes Map
        # 8388608, # aws_cli/boto3
        # 15728640, # s3cmd
        factor_of_1MB(filesize, num_parts) # Used by many clients to upload large files
    ]

    for partsize in filter(possible_partsizes(filesize, num_parts), partsizes):
        _calc_etag, _add_etag = calc_etag(args.inputfile, partsize)
        if args.etag.lower() == _calc_etag.lower() or args.etag.lower() == _add_etag.lower():
            print('Local file matches')
            sys.exit(0)

    print('etag didn\'t validate')
    sys.exit(1)

if __name__ == "__main__":
    main()
